﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolPannelDoor : MonoBehaviour
{

    private bool activable = true;
    [SerializeField] private GameObject doors;


    private void OnTriggerEnter()
    {
        if (!activable)
        {
            return;
        }

        //check no wrong button pressed
        if (GameObject.FindWithTag("b2").GetComponent<UseButton>().buttonPush || GameObject.FindWithTag("b3").GetComponent<UseButton>().buttonPush ||
                    GameObject.FindWithTag("b5").GetComponent<UseButton>().buttonPush || GameObject.FindWithTag("b7").GetComponent<UseButton>().buttonPush ||
                    GameObject.FindWithTag("b9").GetComponent<UseButton>().buttonPush || GameObject.FindWithTag("b10").GetComponent<UseButton>().buttonPush)
        {
            return;
        }

        //check all right buttons pressed
        if (GameObject.FindWithTag("b1").GetComponent<UseButton>().buttonPush && GameObject.FindWithTag("b4").GetComponent<UseButton>().buttonPush &&
        GameObject.FindWithTag("b6").GetComponent<UseButton>().buttonPush && GameObject.FindWithTag("b8").GetComponent<UseButton>().buttonPush && activable)
        {
            doors.GetComponentInChildren<OpenDoors>().AnimateOpenDoors();
            activable = false;

            return;
        }


    }
}
