﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveScenes : MonoBehaviour
{
    [SerializeField] private string scene;
    [SerializeField] private AudioClip sound;
    public GameObject Player;


    private void OnTriggerEnter(Collider other)
    {
        AudioSource.PlayClipAtPoint(sound, transform.position);
        Destroy(Player);
        SceneManager.LoadScene(scene); 
    }
}
