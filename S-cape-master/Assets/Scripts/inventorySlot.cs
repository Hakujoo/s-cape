using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class inventorySlot : MonoBehaviour
{
    public GameObject item = null;
    public GameObject itemInv = null;
    public bool slotOn = false;
    public bool slotReady = false;
    public Vector3 itemScale;
    public int id;

    private void OnTriggerEnter(Collider other)
    {
        // Si dans l'inventaire il n'y a aucun objet :
        // Lors d'une collision avec un objet, on recupère cet objet pour verifier si c'est bien un objet
        // intéragissable. Ensuite on regarde si il est actuellement en main si c'est le cas alors on lui
        // "prépare" la place.
        // Si l'inventaire contient un objet :
        // Lorsque le jouer fait l'action d'interaction, il attrapera l'objet, supprimant celui dans 
        // l'inventaire.
        // Quand l'objet entre, il est enregistré comme actif pour éviter que l'objet soit placé dans plusieurs slots

        ManageInventorySlots.instance.SetSlothUsed(transform.name);

        if (slotOn == false)
        {
            item = other.gameObject;

            if (item.GetComponent<Interactable>() && item.GetComponent<Interactable>().attachedToHand != null)
            {
                slotReady = true;
            }
        }
        else
        {
            if (itemInv.GetComponent<Interactable>().attachedToHand != null)
            {
                Hand hand = itemInv.GetComponent<Interactable>().attachedToHand;
                getItem(hand);
                slotOn = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // mis.ResetSlotUsed();
        // Lorsque on sort de la collision, Reset l'item en "calcul" et remet la preparation du slot à 0
        slotReady = true;
        item = null;
    }

    private void OnTriggerStay(Collider other)
    {
        // On attend que l'objet soit lacher et que la preparation soit pret pour commencer a rentrer
        // l'item dans l'inventaire. Le slot doit être en cours d'utilisation et actif.
        if (item && item.GetComponent<Interactable>() && slotOn == false && slotReady && item.GetComponent<Interactable>().attachedToHand == null
        && ManageInventorySlots.instance.GetSlotUsed() == transform.name)
        {
            PutItem(item);
            slotReady = false;
            slotOn = true;
        }
        else if (slotOn == true && itemInv.GetComponent<Interactable>().attachedToHand != null)
        {
            Hand hand = itemInv.GetComponent<Interactable>().attachedToHand;
            getItem(hand);
            slotOn = false;
        }
    }

    private void PutItem(GameObject item)
    {
        Debug.Log("PutItem");
        item.transform.SetParent(gameObject.transform, false);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = gameObject.transform.rotation;
        itemScale = Vector3.Scale(item.transform.localScale, item.GetComponent<MeshFilter>().sharedMesh.bounds.size);
        itemScale = item.transform.lossyScale;
        item.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        item.GetComponent<Rigidbody>().useGravity = false;
        item.GetComponent<Rigidbody>().isKinematic = true;

        Debug.Log(item);
        itemInv = item;
    }

    private void getItem(Hand hand)
    {
        Debug.Log("GrabItem");
        GrabTypes grabType = hand.GetGrabStarting();
        // item.transform.SetParent(GameObject.Find("item").transform,false);
        hand.DetachObject(itemInv, false);
        itemInv.GetComponent<Rigidbody>().useGravity = true;
        itemInv.GetComponent<Rigidbody>().isKinematic = false;
        itemInv.transform.Translate(Vector3.left);
        itemInv.transform.localScale = new Vector3(1f, 1f, 1f);
        hand.AttachObject(itemInv, grabType);
        itemInv = null;
    }

}
