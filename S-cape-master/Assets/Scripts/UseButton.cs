﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseButton : MonoBehaviour
{
    [SerializeField] private GameObject m_gameObject;
    [SerializeField] private AudioClip sound;
    public bool buttonPush = false;


    private void OnTriggerEnter(Collider other)
    {
        if ( !buttonPush)
        {
            AudioSource.PlayClipAtPoint(sound, transform.position);
            StartCoroutine(PushButtonIn());
            
            buttonPush = true;

        }
        else
        {
            AudioSource.PlayClipAtPoint(sound, transform.position);
            StartCoroutine(PushButtonOut());
            
            
            buttonPush = false;
        }

    }






    private IEnumerator PushButtonIn()
    {
        m_gameObject.GetComponent<BoxCollider>().enabled = false;
        for (int i = 0; i < 6; i++)
        {
            transform.position += new Vector3(0, 0, -0.01f);
            yield return new WaitForSeconds(0.1f);

        }

        yield return new WaitForSeconds(2);
        m_gameObject.GetComponent<BoxCollider>().enabled = true;

    }

    private IEnumerator PushButtonOut()
    {
        m_gameObject.GetComponent<BoxCollider>().enabled = false;
        for (int i = 0; i < 6; i++)
        {
            transform.position -= new Vector3(0, 0, -0.01f);
            yield return new WaitForSeconds(0.1f);

        }

        yield return new WaitForSeconds(2);
        m_gameObject.GetComponent<BoxCollider>().enabled = true;
    }
}
