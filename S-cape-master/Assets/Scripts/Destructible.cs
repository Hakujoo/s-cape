﻿using UnityEngine;

public class Destructible : MonoBehaviour
{
    [SerializeField] GameObject destroyedVersion;
    [SerializeField] AudioClip sound;

    private bool broken;

    private void OnTriggerEnter(Collider collision)
    {
        //En cas de collision avec une arme, on retire l'objet intacte pour le remplacer par la version détruite.
        //La version détruite a un Rigidbody et va donc être soumise à la gravité
        if (collision.CompareTag("Weapon") && !broken)
        {
            broken = true;
            AudioSource.PlayClipAtPoint(sound, transform.position);
            Instantiate(destroyedVersion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }

}
