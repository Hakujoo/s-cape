﻿using UnityEngine;

public class IntroCanvasUI : MonoBehaviour
{
    public GameObject Player;
    public Canvas Canvas;
    public GameObject Pointer;

    private Valve.VR.InteractionSystem.Teleport _teleportScript;

    void Start()
    {
        _teleportScript = Player.GetComponent<Valve.VR.InteractionSystem.Teleport>();
    }

    void Update()
    {
        if (Canvas.enabled)
        {
            _teleportScript.CancelTeleportHint();
            _teleportScript.enabled = false;
        }
        else
        {
            ExitCanvas();
        }
    }

    public void ExitCanvas()
    {
        Canvas.gameObject.SetActive(false);
        Pointer.gameObject.SetActive(false);

        _teleportScript.ShowTeleportHint();
        _teleportScript.enabled = true;
    }
}
